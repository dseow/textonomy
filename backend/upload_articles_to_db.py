from app import db, app
from app.models import Article, Entity
from app.parser import parse_entities
import pandas as pd
import os
import json
import re
import requests
import datetime

def clean_translated(text):
    text = text.replace('&quot;', '')
    text = text.replace('come on', '')
    return text

processed_articles = pd.read_csv("translated_articles.csv")
processed_articles['translated_text'] = processed_articles['translated_text'].apply(clean_translated)

# upload articles into sqlite
with app.app_context():
    for index, row in processed_articles.iterrows():
        print(index)
        entities = eval(row['entities'])
        article = Article(title=row['title'],
                          topic=row['topic'],
                          text=row['text'],
                          datetime=datetime.datetime.strptime(row['datetime'], '%Y-%m-%d'),
                          translated_text=row['translated_text'],
                          summary=row['summarized_text'])
        db.session.add(article)
        db.session.commit()
        for key,ent_lst in entities.items():
            ent_lst = list(set(ent_lst))
            for ent_name in ent_lst:
                if ent_name:
                    ent = Entity(name=ent_name, type=key, origin=article)
                    db.session.add(ent)
                    db.session.commit()

