from transformers import *
from summarizer import Summarizer

custom_config = AutoConfig.from_pretrained('/home/dion/Downloads/work/textonomy/backend/cahya')
custom_config.output_hidden_states = True
custom_tokenizer = AutoTokenizer.from_pretrained('/home/dion/Downloads/work/textonomy/backend/cahya')
custom_model = AutoModel.from_pretrained('/home/dion/Downloads/work/textonomy/backend/cahya',
                                         config=custom_config)

model = Summarizer(custom_model=custom_model, custom_tokenizer=custom_tokenizer)

def summarize_text(text, language="indon"):
    result = model(text)
    return result
