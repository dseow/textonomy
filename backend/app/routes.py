from app import app, db
from app.parser import parse_entities
from app.models import Article, Entity
from app.entity import load_all_entities, clean_entity, get_entity_info
from app.summarize import summarize_text
from app.ner import extract_entities
from flask import request
import fasttext
import json
import requests
import re
from sacremoses import MosesTokenizer
import sentencepiece as spm
import pandas as pd
from dateutil.parser import parse
import dateutil.relativedelta

w2v_model = fasttext.load_model(r'/home/dion/Downloads/work/textonomy/backend/app/fasttext_w2v.bin')
ENT_DF = load_all_entities()

html = re.compile('<.*?>|&lt;.*?&gt;')
mtoken_source = MosesTokenizer(lang='id')
token_source = lambda text: mtoken_source.tokenize(re.sub(html, '', str(text)), return_str=True).strip().lower()

indon_sp = spm.SentencePieceProcessor()
indon_sp.load(r'/home/dion/Downloads/work/textonomy/backend/app/source.model')

eng_sp = spm.SentencePieceProcessor()
eng_sp.load(r'/home/dion/Downloads/work/textonomy/backend/app/target.model')


@app.route('/api/articles', methods=['GET'])
def get_all_articles():
    articles = Article.query.all()
    articles_data = []
    for article in articles:
        article_data = {}
        article_data['title'] = article.title
        article_data['topic'] = article.topic
        article_data['datetime'] = str(article.datetime)
        article_data['id'] = article.id
        articles_data.append(article_data)
    return json.dumps(articles_data)


@app.route('/api/entities', methods=['GET'])
def get_all_entities():
    entities_df = ENT_DF[['Name', 'Type', 'Id']]
    entities_data = entities_df.to_dict('records')
    return json.dumps(entities_data)


@app.route('/api/articles/<int:article_id>')
def get_article(article_id):
    article = Article.query.get(article_id)
    title = article.title
    text = article.text
    translated_text = article.translated_text
    entities = article.entities.all()
    summary = article.summary
    topic = article.topic
    def filter_entities_in_article(article, ent_type):
        return [
                {'name': ent.name, 'id': ent.id}
                for ent in article.entities.filter_by(type=ent_type).all()
                ]
    persons = filter_entities_in_article(article, 'PER')
    locations = filter_entities_in_article(article, 'LOC')
    organizations = filter_entities_in_article(article, 'ORG')
    return json.dumps({"title": title,
                       "topic": topic,
                        "text": text,
                        "translated_text": translated_text,
                        "language": "id",
                        "summary": summary,
                        "persons": persons,
                        "locations": locations,
                        "organizations": organizations})

@app.route('/api/trends', methods=['GET'])
def get_trends():
    date_from = request.args.get('date')
    ent_df = pd.read_csv("/home/dion/Downloads/work/textonomy/backend/cleaned_entities.csv")
    ent_df['datetime'] = pd.to_datetime(ent_df['datetime'])
    ent_df = ent_df[ent_df['Name'] != 'kompas.com']
    start_date = parse(date_from)
    prev_week = start_date - dateutil.relativedelta.relativedelta(weeks=1)
    prev_month = start_date - dateutil.relativedelta.relativedelta(months=1)

    date_res = ""
    df_date = ent_df[ent_df['datetime'] == start_date]
    if not df_date.empty:
        grouped_df_date = df_date.groupby('Name').count().sort_values(by='Type', ascending=False)
        most_common = list(grouped_df_date[:3].index)
        date_res = [{'Name': i, 'Type' : df_date[df_date['Name'] == i]['Type'].value_counts().index[0]} for i in most_common]

    week_res = ""
    df_week = ent_df[(ent_df['datetime'] > prev_week) & (ent_df['datetime'] < start_date)]
    if not df_week.empty:
        grouped_df_week = df_week.groupby('Name').count().sort_values(by='Type', ascending=False)
        most_common = list(grouped_df_week[:3].index)
        week_res = [{'Name': i, 'Type': df_week[df_week['Name'] == i]['Type'].value_counts().index[0]} for i in most_common]

    month_res = ""
    df_month = ent_df[(ent_df['datetime'] > prev_month) & (ent_df['datetime'] < start_date)]
    if not df_month.empty:
        grouped_df_month = df_month.groupby('Name').count().sort_values(by='Type', ascending=False)
        most_common = list(grouped_df_month[:3].index)
        month_res = [{'Name': i, 'Type': df_month[df_month['Name'] == i]['Type'].value_counts().index[0]} for i in most_common]
    print(date_res)
    return json.dumps({'date': date_res, 'week': week_res, 'month': month_res})




@app.route('/api/entities/<int:entity_id>')
def get_entity(entity_id):
    entity_name = Entity.query.filter_by(id=entity_id).first().name
    cleaned_ent = clean_entity(entity_name)
    ent_info = get_entity_info(cleaned_ent, ENT_DF)
    ent_info['Name'] = cleaned_ent
    similar_entities = _search_similar_entities(cleaned_ent)
    ent_info['sim_ents'] = similar_entities
    return ent_info


def _search_similar_entities(word):
    if " " in word:
        word = word.replace(" ", "_")
    sim_words = w2v_model.get_nearest_neighbors(word, k=10)
    words_to_chk = [i[1] for i in sim_words] + [word]
    search_results = []
    for word in words_to_chk:
        if '_' in word:
            word = word.replace('_', ' ')
        query, _ = Article.search(word)
        related_articles = query.all()
        for article in related_articles:
            search_results.append({"word": word,
                                   "id": article.id,
                                   "title": article.title})
    return search_results

@app.route('/api/file-upload', methods=['POST'])
def upload_file():
    uploaded_file = request.files['file']
    if uploaded_file.filename != '':
        text = uploaded_file.read().decode('utf-8')
        translated_text = translate_text(text)
        summary = summarize_text(text)
        ent_text = extract_entities(text)
        parsed_text, entities_in_text = parse_entities(ent_text)
        return json.dumps({"language": "ID",
                           "text": parsed_text,
                           "translated_text": translated_text,
                           "summary": summary})
    else:
        return json.dumps({});


def translate_text(text):
    lines = text.split("\n")
    translated_text = []
    for line in lines:
        if line == "":
            continue
        tokenized_text = token_source(line)
        subworded_text = ['<s>'] + indon_sp.encode_as_pieces(tokenized_text) + ['</s>']
        subworded_text = " ".join([token for token in subworded_text])
        data = json.dumps([{"src": subworded_text, "id": 100}])
        res = requests.post('http://127.0.0.1:5001/translator/translate', data=data)
        translated = res.json()[0][0]['tgt'].split(" ")
        translated = eng_sp.decode_pieces(translated)
        translated_text.append(translated)
    return ("\n").join(translated_text)

