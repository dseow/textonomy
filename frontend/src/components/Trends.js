import React, { useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import TrendCard from './trends/TrendCard.js';
import {
  Grid,
  Popover,
  Button
} from '@material-ui/core';
import DateRangeIcon from '@material-ui/icons/DateRange';
import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import { Calendar } from 'react-date-range';
import axios from 'axios';

export default function Trends() {
  const [anchorEl, setAnchorEl] = useState(null);
  const today = new Date('March 1, 2021 23:15:30');
  const [date, setDate] = useState(today);
  const [trendData, setTrendData] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(`/api/trends?date=${today.toDateString()}`);
      setTrendData(result.data);
    };
 
    fetchData();
  }, []);

  const fetchTrends = async () => {
    const data = await fetch(`/api/trends?date=${date.toDateString()}`);
    const trendData = await data.json();
    setTrendData(trendData);
  }

  const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
  }

  const handleClose = () => {
      setAnchorEl(null);
      fetchTrends();
  }
  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;


  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h5">Trending insights for:</Typography>
            <Grid
              container
              spacing={3}
            > 
              <Grid item>
                <Button 
                  aria-describedby={id}
                  color="inherit"
                  size="small"
                  onClick={handleClick}
                  startIcon={<DateRangeIcon />}
                >
                  {date.toDateString()}
                </Button>
                <Popover
                  id={id}
                  open={open}
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'center',
                  }}
                  transformOrigin={{
                      vertical: 'top',
                      horizontal: 'center',
                  }}
                >
                  <Calendar
                    onChange={item => setDate(item)}
                    date={date}
                  />
                </Popover>
            </Grid>
          </Grid>
        </Grid>
        {Object.keys(trendData).length !== 0 &&
        <>
          {
          trendData.date.length !== 0 &&
          <Grid item xs={12}>
            <Typography variant="h6">Today</Typography>
            <Grid container spacing={5}>
              {trendData.date.map(item => {
                return(<TrendCard word={item.Name} type={item.Type} change={"+100%"} />)
              })}
            </Grid>
          </Grid>
          }
          {
            trendData.week.length !== 0 &&
            <Grid item xs={12}>
            <Typography variant="h6">Last Week</Typography>
            <Grid container spacing={5}>
              {trendData.week.map(item => {
                return(<TrendCard word={item.Name} type={item.Type} change={"+100%"} />)
              })}
            </Grid>
          </Grid>
          }
          {
            trendData.month.length !== 0 &&
            <Grid item xs={12}>
            <Typography variant="h6">Last Month</Typography>
            <Grid container spacing={5}>
              {trendData.month.map(item => {
                return(<TrendCard word={item.Name} type={item.Type} change={"+100%"} />)
              })}
            </Grid>
          </Grid>
          }
        </>
        }
      </Grid>
    </>
  );
}
