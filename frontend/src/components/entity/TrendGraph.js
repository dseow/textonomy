import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
  Label, ResponsiveContainer
} from 'recharts';


export default function TrendGraph(props) {

  return (
    <ResponsiveContainer width={"100%"} height={400}>
      <LineChart
        data={props.data}
        margin={{ top: 30, right: 30, left: 30, bottom: 30 }}
      >
        <CartesianGrid vertical={false} />
        <XAxis dataKey="date"/>
        <YAxis />
        <Tooltip />
        <Line
          dataKey="value"
          name="Occurence"
          stroke="#8884d8"
        />
      </LineChart>
    </ResponsiveContainer>
  );
}
